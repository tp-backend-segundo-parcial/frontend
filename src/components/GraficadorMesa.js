import React, { useRef } from "react";
import * as d3 from "d3";

let radius = 30;
const GraficadorMesa = ({ mesas }) => {
  const svgRef = useRef();
  if (svgRef.current) {
    d3.select("svg g").remove();

    const width = parseInt(d3.select("#mesas").style("width"));
    const height = Math.max(parseInt(d3.select("#mesas").style("height")), 400);
    const maxX = Math.max(
      5,
      d3.max(mesas, (d) => d.x)
    );
    const maxY = Math.max(
      5,
      d3.max(mesas, (d) => d.y)
    );
    const x = d3
      .scaleLinear()
      .domain([0, maxX])
      .range([0, width - 2 * radius]);
    const y = d3
      .scaleLinear()
      .domain([0, maxY])
      .range([0, height - 2 * radius]);
    const svg = d3
      .select(svgRef.current)
      .attr("width", width)
      .attr("height", height)
      .append("g");

    const elem = svg.selectAll("g circle").data(mesas);
    const elemEnter = elem.enter().append("g");
    elemEnter
      .append("circle")
      .attr("id", (d) => `${d.nombre} (${d.x}, ${d.y})`)
      .attr("cx", (d) => x(d.x))
      .attr("cy", (d) => y(d.y))
      .attr("r", radius)
      .attr("data-toggle", "tooltip")
      .attr("data-html", "true")
      .attr("data-placement", "top")
      .style("fill", "#303F9F")
      .attr("title", (d) => d.nombre)
      .on("mouseover", function (d) {
        console.log(d.originalTarget);
        d3.select("#tooltip")
          .transition()
          .duration(200)
          .style("opacity", 1)
          .text(d.originalTarget?.id);
      })
      .on("mouseout", function () {
        d3.select("#tooltip").style("opacity", 0);
      })
      .on("mousemove", function (event) {
        const [x, y] = d3.pointer(event);
        d3.select("#tooltip")
          .style("left", x + 150 + "px")
          .style("top", y + 200 + "px");
      });
  }
  return (
    <div id="mesas">
      <div id="tooltip" style={{ position: "absolute", opacity: 0 }}></div>
      <svg ref={svgRef}></svg>
    </div>
  );
};

export default GraficadorMesa;
