import {
    Container,
    Card,
    CardContent,
    Typography,
    Grid,
} from "@material-ui/core";
import PersonIcon from "@material-ui/icons/Person";
import "../App.css";

const MesaCard = ({ mesa, updateInputs, elegida, setElegida }) => {
    return (
        <Card
            variant="outlined"
            className="card mb-3"
            onClick={() => {
                setElegida(mesa.id);
                updateInputs("mesa", mesa);
            }}
            style={{
                borderColor: `${elegida === mesa.id ? "#f2be2a" : ""}`,
            }}
        >
            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography gutterBottom variant="subtitle1">
                                    {mesa.nombre}
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    Piso: {mesa.nroPiso}
                                </Typography>
                                <Typography
                                    variant="body2"
                                    color="textSecondary"
                                >
                                    Posición: ({mesa.posicion.x},{" "}
                                    {mesa.posicion.y})
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Container style={{ display: "flex", padding: 0 }}>
                            <Typography variant="subtitle1">
                                {mesa.capacidad}
                            </Typography>
                            <PersonIcon />
                        </Container>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    );
};

export default MesaCard;
