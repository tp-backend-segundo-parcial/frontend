import React from "react";
import { Box, Container, Grid, Typography } from "@material-ui/core";
const StepThree = ({ inputs }) => {
    const getDate = () => {
        const date = inputs.fecha;
        return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
    };
    return (
        <Container maxWidth="sm">
            <Box className="text-center title-container">
                <Box className="title-1">Confirmá tus datos</Box>
                <Box className="caption">
                    Verificá que todos los datos estén correctos y luego apretá
                    el botón de finalizar.
                </Box>
            </Box>
            <Container maxWidth="sm">
                <div className="confirmation-card">
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <Typography
                                variant="h6"
                                style={{ fontWeight: 600 }}
                            >
                                {inputs.mesa.nombre}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="overline">
                                {inputs.restaurante.nombre}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="overline">
                                {inputs.nombre} {inputs.apellido}
                            </Typography>
                        </Grid>
                        <Grid item xs={6}></Grid>
                        <Grid item xs={2}>
                            <Typography variant="overline">
                                {inputs.hora_inicio}hs.
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Typography variant="overline">
                                {getDate()}
                            </Typography>
                        </Grid>
                    </Grid>
                </div>
            </Container>
        </Container>
    );
};

export default StepThree;
