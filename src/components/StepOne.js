import { React, useState, useEffect } from "react";
import {
  InputAdornment,
  FormControl,
  Box,
  Container,
  Button,
  OutlinedInput,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DateFnsUtils from "@date-io/date-fns";
import esLocale from "date-fns/locale/es";
import Grid from "@material-ui/core/Grid";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import AddUsuarioDialog from "../components/AddUsuarioDialog";
import UsuarioCard from "../components/UsuarioCard";
import CircularProgress from "@material-ui/core/CircularProgress";
import API from "../api/api";

const StepOne = ({ inputs, updateInputs, setInputs }) => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [restaurantes, setRestaurantes] = useState([]);
  const checkCi = () => {
    return inputs.ci === "";
  };

  const getUser = async () => {
    setIsLoading(true);
    try {
      const response = await API.getUserByCi(inputs.ci);
      if (response.length > 0) {
        setInputs({
          ...inputs,
          nombre: response[0].nombre,
          apellido: response[0].apellido,
          id: response[0].id,
        });
      } else {
        setIsDialogOpen(true);
      }
    } catch (e) {
      console.log(e);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await API.getRestaurantes();
        setRestaurantes(response);
      } catch (e) {
        console.log(e);
      }
    }
    fetchData();
  }, []);
  return (
    <Box>
      <Container maxWidth="sm">
        <Box className="text-center title-container">
          <Box className="title-1">Agrega los datos de tu reserva</Box>
          <Box className="caption">
            Pero antes verifica si estás registrado, si no lo estás podes
            hacerlo directamente desde aquí
          </Box>
        </Box>
      </Container>
      {isLoading ? (
        <Container maxWidth="sm">
          <CircularProgress />
        </Container>
      ) : (
        <Container maxWidth="sm">
          <Box className="mb-10">
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="ci-input">Agregar C.I.</InputLabel>
              <OutlinedInput
                id="ci-input"
                type="text"
                required
                placeholder="Cédula de Identidad"
                label="Agregar C.I."
                value={inputs.ci}
                onChange={(event) => updateInputs("ci", event.target.value)}
                endAdornment={
                  <InputAdornment position="end">
                    <Button
                      style={{ zIndex: 3 }}
                      variant="contained"
                      disabled={checkCi()}
                      onClick={getUser}
                    >
                      <AddIcon
                        style={{
                          marginRight: "3px",
                        }}
                      />
                      agregar
                    </Button>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Box>
          <Box className="mb-5">
            <Grid container spacing={5}>
              <Grid item xs={6}>
                <InputLabel id="restaurante-label">Restaurante</InputLabel>
                <Select
                  fullWidth
                  labelId="restaurante-laber"
                  id="restaurante-select"
                  value={inputs.restaurante}
                  onChange={(event) =>
                    updateInputs("restaurante", event.target.value)
                  }
                >
                  {restaurantes.map((element, index) => {
                    return (
                      <MenuItem value={element} key={element.id}>
                        {element.nombre}
                      </MenuItem>
                    );
                  })}
                </Select>
              </Grid>
              <Grid item xs={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
                  <KeyboardDatePicker
                    autoOk
                    disableToolbar
                    variant="inline"
                    label="Fecha"
                    format="dd/MM/yyyy"
                    value={inputs.fecha}
                    onChange={(event) => updateInputs("fecha", event)}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={6}>
                <InputLabel id="hour-label">Hora de Inicio</InputLabel>
                <Select
                  fullWidth
                  labelId="hour-laber"
                  id="hour-select"
                  value={inputs.hora_inicio}
                  onChange={(event) =>
                    updateInputs("hora_inicio", event.target.value)
                  }
                >
                  <MenuItem value={12}>12</MenuItem>
                  <MenuItem value={13}>13</MenuItem>
                  <MenuItem value={14}>14</MenuItem>
                  <MenuItem value={19}>19</MenuItem>
                  <MenuItem value={20}>20</MenuItem>
                  <MenuItem value={21}>21</MenuItem>
                  <MenuItem value={22}>22</MenuItem>
                </Select>
              </Grid>
              <Grid item xs={6}>
                <InputLabel id="hour-label">Hora de Fin</InputLabel>
                <Select
                  fullWidth
                  labelId="hour-laber"
                  id="hour-select"
                  value={inputs.hora_fin}
                  onChange={(event) => {
                    updateInputs("hora_fin", event.target.value);
                  }}
                >
                  {inputs.hora_inicio < 16
                    ? [13, 14, 15].map((t) => (
                        <MenuItem key={t} value={t}>
                          {t}
                        </MenuItem>
                      ))
                    : [20, 21, 22, 23].map((t) => (
                        <MenuItem key={t} value={t}>
                          {t}
                        </MenuItem>
                      ))}
                </Select>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  id="capacidad"
                  label="Capacidad de la Mesa"
                  type="text"
                  value={inputs.capacidad || ""}
                  onChange={(e) =>
                    updateInputs("capacidad", Number(e.target.value))
                  }
                />
              </Grid>
            </Grid>
          </Box>
          <Box
            style={{
              visibility: `${!inputs.id ? "hidden" : "visible"}`,
            }}
          >
            <UsuarioCard inputs={inputs} />
          </Box>
        </Container>
      )}
      <AddUsuarioDialog
        inputs={inputs}
        updateInputs={updateInputs}
        isOpen={isDialogOpen}
        handleClose={() => setIsDialogOpen((prev) => !prev)}
      />
    </Box>
  );
};

export default StepOne;
