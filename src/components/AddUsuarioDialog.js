import { React, useState } from "react";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    TextField,
} from "@material-ui/core";
import API from "../api/api";

const AddUsuarioDialog = ({ inputs, updateInputs, isOpen, handleClose }) => {
    const [user, setUser] = useState({
        nombre: "",
        apellido: "",
    });
    const onSubmit = async () => {
        try {
            const response = await API.createUser(inputs);
            if (response) {
                updateInputs("id", response.id);
                setUser({
                    nombre: "",
                    apellido: "",
                });
                handleClose();
            }
        } catch (e) {
            console.log(e);
        }
    };
    return (
        <Dialog open={isOpen} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Agregar Usuario</DialogTitle>
            <DialogContent>
                <Grid container alignContent={"center"} spacing={2}>
                    <Grid item sm={6}>
                        <TextField
                            fullWidth
                            autoFocus
                            id="nombre"
                            label="Nombre"
                            type="text"
                            value={user.nombre}
                            onChange={(event) => {
                                setUser({
                                    ...user,
                                    nombre: event.target.value,
                                });
                                updateInputs("nombre", event.target.value);
                            }}
                        />
                    </Grid>
                    <Grid item sm={6}>
                        <TextField
                            fullWidth
                            id="apellido"
                            label="Apellido"
                            type="text"
                            value={user.apellido}
                            onChange={(event) => {
                                setUser({
                                    ...user,
                                    apellido: event.target.value,
                                });
                                updateInputs("apellido", event.target.value);
                            }}
                        />
                    </Grid>

                    <Grid item sm={6}>
                        <TextField
                            fullWidth
                            id="ci"
                            label="Número de cédula"
                            type="text"
                            value={inputs.ci}
                            onChange={(event) => {
                                updateInputs("ci", event.target.value);
                            }}
                        />
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Cancelar
                </Button>
                <Button color="primary" onClick={onSubmit}>
                    Guardar
                </Button>
            </DialogActions>
        </Dialog>
    );
};
export default AddUsuarioDialog;
