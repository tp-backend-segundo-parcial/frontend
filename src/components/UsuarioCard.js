import React from "react";
import { Grid, Paper, Typography, Avatar } from "@material-ui/core";

const UsuarioCard = ({ inputs }) => {
    return (
        <div>
            <Paper elevation={3}>
                <Grid container spacing={2} style={{ padding: 10 }}>
                    <Grid item style={{ alignSelf: "center" }}>
                        <Avatar>
                            {inputs?.nombre[0]}
                            {inputs?.apellido[0]}
                        </Avatar>
                    </Grid>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography gutterBottom variant="subtitle1">
                                    {inputs.nombre} {inputs.apellido}
                                </Typography>
                                <Typography
                                    variant="body2"
                                    color="textSecondary"
                                >
                                    CI: {inputs.ci}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </div>
    );
};

export default UsuarioCard;
