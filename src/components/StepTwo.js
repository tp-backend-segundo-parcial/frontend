import { React, useState, useEffect } from "react";
import { Container, Box } from "@material-ui/core";
import MesaCard from "./MesaCard";
import API from "../api/api";
import "../App.css";

const StepTwo = ({ inputs, updateInputs }) => {
  const [mesas, setMesas] = useState([]);
  const [elegida, setElegida] = useState(null);
  useEffect(() => {
    async function fetchData() {
      const response = await API.getMesasDisponibles(
        inputs.restaurante.id,
        inputs.fecha,
        inputs.hora_inicio,
        inputs.hora_fin,
        inputs.capacidad
      );
      setMesas(response);
    }
    fetchData();
  }, []);

  return (
    <Box>
      <Container maxWidth="sm">
        <Box className="text-center title-container">
          <Box className="title-1">Elegí tu mesa</Box>
          <Box className="caption">
            La mesa que elijas quedará reservada durante el horario que elegiste
            previamente
          </Box>
        </Box>
      </Container>
      <Container maxWidth="sm">
        <Box className="title-1 mb-5">Mesas Disponibles</Box>
        <Box
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignContent: "flex-start",
            flexWrap: "wrap",
          }}
        >
          {mesas.map((element) => {
            return (
              <MesaCard
                key={element.id}
                mesa={element}
                updateInputs={updateInputs}
                setElegida={setElegida}
                elegida={elegida}
              />
            );
          })}
        </Box>
      </Container>
    </Box>
  );
};

export default StepTwo;
