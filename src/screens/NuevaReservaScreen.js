import React, { useState } from "react";
import {
  Fab,
  Box,
  Container,
  Stepper,
  Step,
  StepLabel,
  Button,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ArrowFowardIcon from "@material-ui/icons/ArrowForward";
import CheckIcon from "@material-ui/icons/Check";
import StepOne from "../components/StepOne";
import StepTwo from "../components/StepTwo";
import StepThree from "../components/StepThree";
import API from "../api/api";
import Confirmation from "../images/confirmation.svg";
import "../App.css";

const NuevaReservaScreen = ({ history }) => {
  const [stepsDone, setStepsDone] = useState(0);
  const [success, setSuccess] = useState(false);
  const [inputs, setInputs] = useState({
    ci: "",
    restaurante: "",
    nombre: "",
    apellido: "",
    fecha: new Date(),
    hora_inicio: "",
    hora_fin: "",
    capacidad: 0,
    mesa: null,
  });

  const updateInputs = (field, value) => {
    setInputs({ ...inputs, [field]: value });
  };

  const back = () => {
    if (stepsDone !== 0) {
      setStepsDone(stepsDone - 1);
    } else {
      console.log(history);
      history.goBack();
    }
  };

  const foward = () => {
    if (stepsDone < 2) {
      setStepsDone(stepsDone + 1);
    }
  };
  const createReserva = () => {
    API.createReserva(inputs).then(() => {
      setSuccess(true);
    });
  };
  const renderStepPage = () => {
    switch (stepsDone) {
      case 0:
        return (
          <StepOne
            inputs={inputs}
            updateInputs={updateInputs}
            setInputs={setInputs}
          />
        );

      case 1:
        return <StepTwo inputs={inputs} updateInputs={updateInputs} />;
      case 2:
        return <StepThree inputs={inputs} />;
      default:
        return <Box>Def</Box>;
    }
  };
  if (!success) {
    return (
      <Box>
        <ArrowBackIcon className="back-button" onClick={back} />
        <Fab
          // disabled={this.disableFabButton()}
          color="primary"
          aria-label="add"
          className="foward-button"
          onClick={foward}
        >
          {stepsDone < 2 ? (
            <ArrowFowardIcon />
          ) : (
            <CheckIcon onClick={createReserva} />
          )}
        </Fab>
        <Container maxWidth="sm">
          <Box className="stepper-container mb-1">
            <Stepper activeStep={stepsDone} className="yellowStepper">
              <Step key="1" completed={false}>
                <StepLabel />
              </Step>
              <Step key="2" completed={false}>
                <StepLabel />
              </Step>
              <Step key="3" completed={false}>
                <StepLabel />
              </Step>
            </Stepper>
          </Box>
        </Container>
        {renderStepPage()}
      </Box>
    );
  } else {
    return (
      <Box>
        <Container maxWidth="sm">
          <Box
            style={{
              display: "flex",
              justifyContent: "center",
              padding: 50,
            }}
          >
            <img src={Confirmation} style={{ width: 100, height: "auto" }} />
          </Box>
          <Box className="text-center title-container">
            <Box className="title-1">¡Hemos guardado tu reserva!</Box>
            <Box className="caption">
              Podes realizar otras reservas usando el botón de abajo
            </Box>
          </Box>
        </Container>
        <Container
          maxWidth="sm"
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button
            color="primary"
            variant="contained"
            onClick={() => {
              setStepsDone(0);
              setInputs({
                ci: "",
                restaurante: "",
                nombre: "",
                apellido: "",
                fecha: new Date(),
                hora_inicio: "",
                hora_fin: "",
                mesa: null,
              });
              setSuccess(false);
            }}
          >
            Realizar otra reserva
          </Button>
        </Container>
      </Box>
    );
  }
};

export default NuevaReservaScreen;
