import React, { useEffect, useState } from "react";
import {
    Box,
    Button,
    Container,
    Fab,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
} from "@material-ui/core";
import { DataGrid } from "@material-ui/data-grid";
import AddIcon from "@material-ui/icons/Add";
import DateFnsUtils from "@date-io/date-fns";
import esLocale from "date-fns/locale/es";
import api from "../api/api";
import {
    KeyboardDatePicker,
    MuiPickersUtilsProvider,
} from "@material-ui/pickers";

import { Link } from "react-router-dom";
const COLUMNAS = [
    {
        field: "mesa",
        headerName: "Mesa",
        renderCell: ({ row: item }) => item.mesa.nombre,
        flex: 1,
    },
    {
        field: "usuario",
        headerName: "Usuario",
        renderCell: ({ row: item }) => {
            console.log(item);
            return item.usuario.nombre + " " + item.usuario.apellido;
        },
        flex: 1,
    },
    { field: "horaDesde", headerName: "Hora de Inicio", flex: 1 },
    { field: "horaHasta", headerName: "Hora de Fin", flex: 1 },
];
const COLUMNAS_RESTAURANTES = [
    {
        field: "nombre",
        headerName: "Nombre",
        flex: 1,
    },
    { field: "direccion", headerName: "Dirección", flex: 1 },
    {
        field: "actions",
        headerName: "Acción",
        flex: 1,
        align: "center",
        renderCell: ({ row: item }) => (
            <Button
                component={Link}
                variant={"outlined"}
                to={`/restaurantes/${item.id}`}
            >
                Ver Mesas
            </Button>
        ),
    },
];
const ListadoReservaScreen = ({}) => {
    const [reservas, setReservas] = useState([]);
    const [filtros, setFiltros] = useState({ fecha: new Date() });
    const [restaurantes, setRestaurantes] = useState([]);
    useEffect(() => {
        (async () => {
            const data = await api.getRestaurantes();
            setRestaurantes(data);
        })();
    }, []);

    const onSubmit = async () => {
        try {
            const response = await api.getReservas(
                filtros.restaurante.id,
                filtros.fecha,
                filtros.cliente
            );
            setReservas(response);
        } catch (e) {
            console.log(e);
            setRestaurantes([]);
        }
    };
    return (
        <Box style={{ marginTop: 50 }}>
            <Fab
                to={"/nueva-reserva"}
                component={Link}
                color="primary"
                aria-label="add"
                className="foward-button"
            >
                <AddIcon />
            </Fab>
            <Container maxWidth={"md"}>
                <Box className="text-center title-container">
                    <Box className="title-1">Listado de Reservas</Box>
                </Box>
                <Grid container>
                    <Grid item style={{ padding: "16px" }} md={"4"}>
                        <InputLabel id="restaurante-label">
                            Restaurante
                        </InputLabel>
                        <Select
                            fullWidth
                            labelId="restaurante-laber"
                            id="restaurante-select"
                            value={filtros.restaurante}
                            onChange={(event) =>
                                setFiltros((prev) => ({
                                    ...prev,
                                    restaurante: event.target.value,
                                }))
                            }
                        >
                            {restaurantes.map((element) => {
                                return (
                                    <MenuItem value={element} key={element.id}>
                                        {element.nombre}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </Grid>
                    <Grid item md={"3"} style={{ padding: "16px" }}>
                        <TextField
                            fullWidth
                            id="cliente"
                            label="CI Cliente"
                            type="text"
                            value={filtros.cliente || ""}
                            onChange={(e) =>
                                setFiltros((prev) => ({
                                    ...prev,
                                    cliente: e.target.value,
                                }))
                            }
                        />
                    </Grid>
                    <Grid item md={"3"} style={{ padding: "16px" }}>
                        <MuiPickersUtilsProvider
                            utils={DateFnsUtils}
                            locale={esLocale}
                        >
                            <KeyboardDatePicker
                                autoOk
                                disableToolbar
                                variant="inline"
                                label="Fecha"
                                format="dd/MM/yyyy"
                                value={filtros.fecha}
                                onChange={(event) => {
                                    console.log(event);
                                    setFiltros((prev) => ({
                                        ...prev,
                                        fecha: event,
                                    }));
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid
                        item
                        container
                        md={"2"}
                        style={{
                            padding: "16px",
                        }}
                        justify={"flex-end"}
                        alignItems={"flex-end"}
                    >
                        <Button
                            variant={"contained"}
                            color={"primary"}
                            disabled={!filtros.restaurante}
                            onClick={onSubmit}
                        >
                            Buscar
                        </Button>
                    </Grid>
                </Grid>

                <Box>
                    <DataGrid
                        columnTypes={[]}
                        autoHeight
                        density={"standard"}
                        columns={COLUMNAS}
                        rows={reservas}
                        pageSize={10}
                        sortingOrder={"asc"}
                        columnBuffer={1}
                    />
                </Box>

                <Box
                    className="text-center title-container"
                    style={{ marginTop: "50px" }}
                >
                    <Box className="title-1">Listado de Restaurantes</Box>
                </Box>

                <Box>
                    <DataGrid
                        columnTypes={[]}
                        autoHeight
                        density={"standard"}
                        columns={COLUMNAS_RESTAURANTES}
                        rows={restaurantes}
                        pageSize={10}
                        sortingOrder={"asc"}
                        columnBuffer={1}
                    />
                </Box>
            </Container>
        </Box>
    );
};
export default ListadoReservaScreen;
