import React, { useEffect, useState } from "react";
import {
  Box,
  Container,
  Button,
  Grid,
  Select,
  MenuItem,
  InputAdornment,
  InputLabel,
  TextField,
  Table,
  TableContainer,
  TableCell,
  TableRow,
  TableBody,
  TableHead,
  Paper,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import AddIcon from "@material-ui/icons/Add";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import API from "../api/api";
import { Link } from "react-router-dom";
import AddUsuarioDialog from "../components/AddUsuarioDialog";
const ConsumoScreen = ({ history, location }) => {
  const [consumo, setConsumo] = useState([]);
  const [mesa, setMesa] = useState({});
  const [clientes, setClientes] = useState([]);
  const [selectedCliente, setSelectedCliente] = useState(null);
  const [categorias, setCategorias] = useState([]);
  const [selectedCategoria, setSelectedCategoria] = useState([]);
  const [productos, setProductos] = useState([]);
  const [selectedProducto, setSelectedProducto] = useState([]);
  const [cantidad, setCantidad] = useState(0);
  const [productosAgregados, setProductosAgregados] = useState([]);
  const [total, setTotal] = useState(0);
  const [productosConsumo, setProductosConsumo] = useState([]);
  const [inputs, setInputs] = useState({
    ci: "",
    nombre: "",
    apellido: "",
  });
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const updateInputs = (field, value) => {
    setInputs({ ...inputs, [field]: value });
  };
  const back = () => {
    history.goBack();
  };

  const agregarProducto = () => {
    setProductosAgregados([
      ...productosAgregados,
      {
        producto: selectedProducto,
        cantidad: cantidad,
        precio: selectedProducto.precio,
      },
    ]);
    const nuevoTotal = parseInt(total) + selectedProducto.precio * cantidad;
    setTotal(nuevoTotal);
    setSelectedProducto([]);
    setCantidad(0);
  };

  const onSubmit = async () => {
    if (consumo.length === 0) {
      try {
        const consumo = await API.createConsumo(
          mesa.id,
          selectedCliente.id,
          mesa.restauranteId
        );
        setConsumo(consumo);
        for (let i in productosAgregados) {
          const detalle = await API.createDetalleConsumo(
            mesa.id,
            mesa.restauranteId,
            productosAgregados[i].producto.id,
            productosAgregados[i].cantidad
          );
        }
      } catch (e) {
        console.log(e);
      }
    } else {
      let a = productosConsumo;
      try {
        const consumo = await API.updateConsumo(
          mesa.id,
          selectedCliente.id,
          mesa.restauranteId
        );
        setConsumo(consumo);
        for (let i in productosAgregados) {
          const detalle = await API.createDetalleConsumo(
            mesa.id,
            mesa.restauranteId,
            productosAgregados[i].producto.id,
            productosAgregados[i].cantidad
          );
          a.push({
            nombre: productosAgregados[i].producto.nombre,
            cantidad: productosAgregados[i].cantidad,
            precio: productosAgregados[i].precio,
          });
        }
        setProductosConsumo(a);
        setProductosAgregados([]);
      } catch (e) {
        console.log(e);
      }
    }
  };

  const cerrarCuenta = async () => {
    try {
      const response = await API.deleteConsumo(mesa.id, mesa.restauranteId);
      const pdf = await API.getFactura(consumo.id);
      const file = new Blob([pdf], { type: "application/pdf" });
      const fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    let a = [];
    async function fetchData() {
      try {
        const clientes = await API.getUsers();
        const categoriasData = await API.getCategorias();
        const consumoData = await API.getConsumo(
          location.state.mesa.id,
          location.state.mesa.restauranteId
        );
        setClientes(clientes);
        setCategorias(categoriasData);
        if (consumoData) {
          setConsumo(consumoData);
          setSelectedCliente(consumoData.usuario);
          for (let i in consumoData.detalle_consumos) {
            const p = await API.getProducto(
              consumoData.detalle_consumos[i].productoId
            );
            a.push({
              nombre: p.nombre,
              cantidad: consumoData.detalle_consumos[i].cantidad,
              precio: p.precio,
            });
          }
          setProductosConsumo(a);
          setTotal(consumoData.total);
        }
      } catch (e) {
        console.log(e);
      }
    }
    setMesa(location.state.mesa);
    fetchData();
  }, [isDialogOpen]);

  useEffect(() => {
    setProductos(selectedCategoria.productos);
  }, [selectedCategoria]);

  return (
    <Box>
      <ArrowBackIcon className="back-button" onClick={back} />
      <Container maxWidth="sm" className="stepper-container">
        <Box className="text-center title-container">
          <Box className="title-1">{mesa.nombre}</Box>
          <Box className="caption">
            Podes agregar productos o cerrar tu cuenta.
          </Box>
        </Box>
      </Container>
      <Container maxWidth="md" className="mb-5">
        <Grid container spacing={5} justify="center" alignItems="center">
          <Grid item xs={5}>
            <Autocomplete
              options={clientes}
              value={selectedCliente}
              getOptionLabel={(option) => `${option.nombre} ${option.apellido}`}
              renderInput={(params) => (
                <TextField {...params} label="Cliente" variant="outlined" />
              )}
              onChange={(e, value) => setSelectedCliente(value)}
            />
          </Grid>
          {/* <Grid item xs={4}>
                        <InputLabel id="cliente-label">Cliente</InputLabel>
                        <Select
                            fullWidth
                            labelId="clente-label"
                            value={selectedCliente}
                            renderValue={(value) =>
                                `${value.nombre} ${value.apellido}`
                            }
                            onChange={(e) => {
                                setSelectedCliente(e.target.value);
                            }}
                        >
                            {clientes.map((cliente) => (
                                <MenuItem key={cliente.id} value={cliente}>
                                    {cliente.nombre} {cliente.apellido}
                                </MenuItem>
                            ))}
                        </Select>
                    </Grid> */}
          <Grid item xs={4}>
            <InputAdornment position="end">
              <Button
                style={{ zIndex: 3 }}
                variant="contained"
                color="primary"
                onClick={() => {
                  setIsDialogOpen(true);
                }}
              >
                <AddIcon
                  style={{
                    marginRight: "3px",
                  }}
                />
                nuevo
              </Button>
            </InputAdornment>
          </Grid>
        </Grid>
      </Container>
      <Container maxWidth="md" className="mb-2">
        <Grid
          container
          spacing={5}
          justify="center"
          alignItems="flex-end"
          style={{ marginBottom: 20 }}
        >
          <Grid item xs={4}>
            <InputLabel id="categoria-label">
              Categorías de productos
            </InputLabel>
            <Select
              labelId="categoria-label"
              value={selectedCategoria}
              fullWidth
              onChange={(event) => {
                setSelectedCategoria(event.target.value);
              }}
            >
              {categorias.map((categoria) => (
                <MenuItem key={categoria.id} value={categoria}>
                  {categoria.nombre}
                </MenuItem>
              ))}
            </Select>
          </Grid>
          <Grid item xs={4}>
            <InputLabel id="productos-label">Productos</InputLabel>
            <Select
              labelId="productos-label"
              value={selectedProducto}
              fullWidth
              onChange={(event) => {
                setSelectedProducto(event.target.value);
              }}
            >
              {productos?.map((producto) => (
                <MenuItem key={producto.id} value={producto}>
                  {producto.nombre}
                </MenuItem>
              ))}
            </Select>
          </Grid>
          <Grid item xs={4}>
            <TextField
              fullWidth
              id="cantidad"
              label="Cantidad"
              type="number"
              value={cantidad}
              onChange={(e) => {
                setCantidad(e.target.value);
              }}
            />
          </Grid>
        </Grid>
        <Box style={{ textAlign: "center" }}>
          <Button variant="contained" color="primary" onClick={agregarProducto}>
            Agregar
          </Button>
        </Box>
      </Container>
      <Container maxWidth="md" className="mb-3">
        <TableContainer component={Paper}>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>Producto</TableCell>
                <TableCell align="right">Cantidad</TableCell>
                <TableCell>Precio Unitario</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {productosConsumo.map((e, index) => (
                <TableRow key={e.cantidad + index}>
                  <TableCell>{e.nombre}</TableCell>
                  <TableCell align="right">{e.cantidad}</TableCell>
                  <TableCell align="right">{e.precio} Gs.</TableCell>
                </TableRow>
              ))}
              {productosAgregados.map((e) => (
                <TableRow key={e.producto.id}>
                  <TableCell>{e.producto.nombre}</TableCell>
                  <TableCell align="right">{e.cantidad}</TableCell>
                  <TableCell align="right">{e.precio} Gs.</TableCell>
                </TableRow>
              ))}
              <TableRow>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell align="right">Total {total} Gs.</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
      <Container maxWidth="xs">
        <Grid container spacing={5} justify={"space-around"}>
          <Grid item xs={6}>
            <Button variant="contained" color="primary" onClick={onSubmit}>
              Aceptar
            </Button>
          </Grid>
          <Grid item xs={6} align="end">
            <Button
              component={Link}
              variant="contained"
              color="primary"
              onClick={cerrarCuenta}
              to={"/"}
            >
              Cerrar cuenta
            </Button>
          </Grid>
        </Grid>
      </Container>
      <AddUsuarioDialog
        inputs={inputs}
        updateInputs={updateInputs}
        isOpen={isDialogOpen}
        handleClose={() => setIsDialogOpen((prev) => !prev)}
      />
    </Box>
  );
};
export default ConsumoScreen;
