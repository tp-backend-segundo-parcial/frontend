import React, { useEffect, useMemo, useState } from "react";
import {
  Box,
  Container,
  Step,
  StepLabel,
  Stepper,
  MenuItem,
  Select,
  Button,
} from "@material-ui/core";
import GraficadorMesa from "../components/GraficadorMesa";
import { useParams } from "react-router";
import api from "../api/api";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";

const DisposicionMesasScreen = ({ history }) => {
  const { id } = useParams();
  const [restaurante, setRestaurante] = useState(undefined);
  const [selectedPiso, setSelectedPiso] = useState(0);
  const [selectedMesa, setSelectedMesa] = useState(null);
  useEffect(() => {
    (async () => {
      try {
        const data = await api.getRestaurante(id);
        setRestaurante(data);
      } catch (e) {
        console.log(e);
      }
    })();
  }, []);
  const pisos = useMemo(() => {
    const pisos = new Set();
    if (restaurante) {
      restaurante.mesas.forEach((mesa) => {
        pisos.add(mesa.nroPiso);
      });
    }
    return [...pisos];
  }, [restaurante]);

  return (
    <Box style={{ marginBottom: "50px" }}>
      <ArrowBackIcon className="back-button" onClick={() => history.goBack()} />
      <Container maxWidth={"md"}>
        <Box className="text-center title-container" style={{ marginTop: 50 }}>
          <Box className="title-1">{restaurante?.nombre}</Box>
          <Box style={{ margin: "10px 0px" }}>Distribucion de Mesas.</Box>
        </Box>
        <Box className="mb-1" style={{ alignContent: "center" }}>
          <Stepper
            activeStep={selectedPiso}
            className="yellowStepper"
            alternativeLabel
          >
            {pisos.map((p) => (
              <Step
                key={p}
                onClick={() => setSelectedPiso(Number(p) - 1)}
                completed={false}
              >
                <StepLabel>{`Piso ${p}`}</StepLabel>
              </Step>
            ))}
          </Stepper>
        </Box>
        <Box
          style={{
            border: "0.5px solid #cbcbfa",
            borderRadius: "10px",
            padding: "16px",
            margin: "16px 0px",
          }}
        >
          <Grid item container justify={"center"}>
            <Grid item xs={12} style={{ textAlign: "center" }}>
              <h4>Ver consumo de Mesa</h4>
            </Grid>
            <Grid item xs={6} style={{ margin: "0px 16px" }}>
              <Select
                title={"Seleccione la Mesa"}
                className="mb-5"
                fullWidth
                value={selectedMesa}
                onChange={(event) => {
                  setSelectedMesa(event.target.value);
                }}
              >
                {restaurante?.mesas
                  .filter((mesa) => Number(mesa.nroPiso) === selectedPiso + 1)
                  .map((mesa) => (
                    <MenuItem key={mesa.id} value={mesa}>
                      {mesa.nombre}
                    </MenuItem>
                  )) || []}
              </Select>
            </Grid>
            <Grid item xs={2}>
              <Button
                component={Link}
                variant="contained"
                color="primary"
                disabled={!selectedMesa}
                to={{
                  pathname: `/restaurantes/${selectedMesa?.restauranteId}/mesas/${selectedMesa?.id}`,
                  state: { mesa: selectedMesa },
                }}
              >
                Aceptar
              </Button>
            </Grid>
          </Grid>
        </Box>
        <Box
          style={{
            borderRadius: "10px",
            border: "0.5px solid #cbcbfa",
          }}
        >
          <Grid container spacing={5}>
            <Grid item xs={12} style={{ textAlign: "center" }}>
              <h4>Disposicion de Mesas</h4>
            </Grid>
            <Grid item xs={8}>
              <GraficadorMesa
                mesas={
                  restaurante?.mesas
                    .filter((mesa) => Number(mesa.nroPiso) === selectedPiso + 1)
                    .map((mesa) => ({
                      nombre: mesa.nombre,
                      x: Number(mesa.posicion.x),
                      y: Number(mesa.posicion.y),
                    })) || []
                }
              />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};

export default DisposicionMesasScreen;
