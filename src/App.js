import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import NuevaReservaScreen from "./screens/NuevaReservaScreen";
import ListadoReservasScreen from "./screens/ListadoReservasScreen";
import DisposicionMesasScreen from "./screens/DisposicionMesasScreen";
import ConsumoScreen from "./screens/ConsumoScreen";

function App() {
    return (
        <Router>
            <Switch>
                <Route path={"/nueva-reserva"} component={NuevaReservaScreen} />
                <Route
                    path={"/restaurantes/:id/mesas/:id"}
                    component={ConsumoScreen}
                />
                <Route
                    path={"/restaurantes/:id"}
                    component={DisposicionMesasScreen}
                />
                <Route path={"/"} component={ListadoReservasScreen} />
            </Switch>
        </Router>
    );
}

export default App;
