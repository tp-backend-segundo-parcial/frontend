import axios from "axios";
import config from "./config";

class API {
    async getUserByCi(ci) {
        const response = await axios.get(`${config.serverURL}/usuarios`, {
            params: { cedula: ci },
        });
        return response.data;
    }

    async createUser(data) {
        const response = await axios.post(`${config.serverURL}/usuarios`, {
            nombre: data.nombre,
            apellido: data.apellido,
            cedula: data.ci,
        });
        return response.data;
    }

    async getRestaurantes() {
        const response = await axios.get(`${config.serverURL}/restaurantes`);
        return response.data;
    }

    async getMesasDisponibles(idRestaurante, fecha, desde, hasta, capacidad) {
        const response = await axios.get(
            `${config.serverURL}/restaurantes/${idRestaurante}/mesas/disponibles`,
            {
                params: {
                    fecha: fecha,
                    horaDesde: desde,
                    horaHasta: hasta,
                    capacidad,
                },
            }
        );
        return response.data;
    }

    async createReserva(data) {
        const date = data.fecha;
        const fecha = `${date.getFullYear()}-${
            date.getMonth() + 1
        }-${date.getDate()}`;
        const response = await axios.post(
            `${config.serverURL}/restaurantes/${data.restaurante.id}/reservas/`,
            {
                mesaId: data.mesa.id,
                usuarioId: data.id,
                fecha: fecha,
                horaDesde: data.hora_inicio,
                horaHasta: data.hora_fin,
            }
        );
        return response.data;
    }
    async getRestaurante(id) {
        const response = await axios.get(
            `${config.serverURL}/restaurantes/${id}`
        );
        return response.data;
    }
    async getReservas(restaurante, fecha, cliente) {
        const date = `${fecha.getFullYear()}-${
            fecha.getMonth() + 1
        }-${fecha.getDate()}`;
        const response = await axios.get(
            `${config.serverURL}/restaurantes/${restaurante}/reservas/`,
            {
                params: {
                    fecha: date,
                    ...(cliente ? { usuario: cliente } : {}),
                },
            }
        );
        return response.data;
    }
    async getUsers() {
        const response = await axios.get(`${config.serverURL}/usuarios`);
        return response.data;
    }
    async getCategorias() {
        const response = await axios.get(
            `${config.serverURL}/categorias-productos`
        );
        return response.data;
    }

    async createConsumo(mesaId, usuarioId, restauranteId) {
        const response = await axios.post(
            `${config.serverURL}/restaurantes/${restauranteId}/mesas/${mesaId}/consumo`,
            { usuarioId }
        );
        return response.data;
    }

    async updateConsumo(mesaId, usuarioId, restauranteId) {
        const response = await axios.put(
            `${config.serverURL}/restaurantes/${restauranteId}/mesas/${mesaId}/consumo`,
            { clienteId: usuarioId }
        );
        return response.data;
    }

    async getConsumo(mesaId, restauranteId) {
        const response = await axios.get(
            `${config.serverURL}/restaurantes/${restauranteId}/mesas/${mesaId}/consumo`
        );
        return response.data;
    }

    async createDetalleConsumo(mesaId, restauranteId, productoId, cantidad) {
        const response = await axios.post(
            `${config.serverURL}/restaurantes/${restauranteId}/mesas/${mesaId}/consumo/detalle`,
            { productoId: productoId, cantidad: cantidad }
        );
        return response.data;
    }

    async getProducto(id) {
        const response = await axios.get(`${config.serverURL}/productos/${id}`);
        return response.data;
    }

    async deleteConsumo(mesaId, restauranteId) {
        const response = await axios.delete(
            `${config.serverURL}/restaurantes/${restauranteId}/mesas/${mesaId}/consumo`
        );
        return response.data;
    }

    async getFactura(consumoId) {
        const response = await axios.get(
            `${config.serverURL}/facturas/${consumoId}`,
            { responseType: "blob" }
        );
        return response.data;
    }
}
export default new API();
